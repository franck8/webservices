package fr.toulouse.greta.formation.webservice.fb;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/*@RequestMapping(value = "/V1") */

/*=============== ATTENTION ===================
 * ajouter une requestMapping en tete d'une classe modifie l'URL de chaque services ou pages !!!
 * PERMET UNE MEILLEURE ORGANISATION  - c'est le routage 
 ===========================================*/
@RestController
public class CookBookController {

	@RequestMapping(value = "/complete", method = RequestMethod.GET, produces = "application/JSON") /*
																									 * consumes =
																									 * "application/JSON")
																									 */
	/*
	 * METHODE AVEC RESTRICTION interet technique dans le cas ou l'on utiliserait la
	 * meme url mais avec des methodes differentes
	 * 
	 * consumes/produces sont 2 parametres pour montrer ce que les donnees produise
	 * ou utilise
	 * 
	 */
	public boolean isComplete() {
		return false;
		/*
		 * effort important pour passer du java au JSON
		 */
	}

	/* @GetMapping *//* est l'equivalent du requestMapping avec la methode GET integree */
	@GetMapping(value = "/health", produces = "application/JSON")
	public Map<String, String> getHealth() {
		Map<String, String> res = new HashMap<String, String>();
		res.put("CookBook", "incomplete");
		res.put("Data", "ok");
		res.put("server", "ok");
		System.out.println(res);
		return res;
		/*
		 * les dependances Jackson pour la realisation de conversion de JAVA vers qq
		 * chose ou de qq chose vers JAVA serialisation des donneees ici
		 */

	}

	@GetMapping(value = "/recipes", produces = "application/JSON")
	public static List<Receipe> getReceipes() {

		List<Receipe> Listrec = new ArrayList<>();
		Listrec.add(new Receipe("poulet", 20, "bien cuire"));
		Listrec.add(new Receipe("oeuf dur", 10, "ajouter les oeufs apres ebullition"));
		Listrec.add(new Receipe("cookie", 15, "retirer du four rapidement"));

		/*
		 * il regarde si il y a des getters et il fait 3 attributs JSON pour exposer les
		 * data dans la page html capable - renvoit d'une liste JSON ici dans request
		 * mapping il y a un attribut par defaut (value)
		 */
		return Listrec;

	}

	@GetMapping(value = "/complete.xml", produces = "text/xml")
	public boolean isCompleteXml() {
		return false;
	}

	@GetMapping(value = "/recipes/{n}", produces = "application/JSON")
	public Receipe getReceipe(@PathVariable("n") int n) {

		return getReceipes().get(n);
	}
	/* ATTENTION ========== !!!! Dans cet exemple le {n} est lie au ("n") !!! */

	@DeleteMapping(value = "/recipes/{n}", produces = "application/JSON")
	public void deleteReceipe(@PathVariable("n") int n) {

		System.out.println("Recette supprimee:" + n);

	}

	@PostMapping(value = "/recipes", produces = "application/JSON")
	public void newReceipe(@RequestBody Receipe r) {

		System.out.println(r.getName());
	}

}
