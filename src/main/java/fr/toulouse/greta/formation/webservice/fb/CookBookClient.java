package fr.toulouse.greta.formation.webservice.fb;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.web.client.RestTemplate;

public class CookBookClient {

	public static void main(String[] args) {
		try {
			RestTemplate rt = new RestTemplate();
			boolean complete = rt.getForObject("http://localhost:8085/complete", boolean.class);
			System.out.println("complet?" + complete);
			System.out.println("Recette 0" + rt.getForObject("http://localhost:8085/receipes/1", Receipe.class).getName());
			System.out.println("WP" + rt.getForObject("https://fr.wikipedia.org/w/api.php?action=query&list=search&format=json&srsearch=choucroute", LinkedHashMap.class)); 
			rt.delete("http://localhost:8085/receipes/2");
			rt.postForLocation("http://localhost:8085/receipes", new Receipe("Glace", 3, "ne pas laisser fondre"));
			/*
			On avait Objet.class a la base avant de placer Map
			jsonfm (au lieu de jason) permet un code propre ;
			 */

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
