package fr.toulouse.greta.formation.webservice.fb;

public class Receipe {

	private String name;
	private int duration;
	private String text;

	public Receipe() {
		this(null, 0, null);
	}

	public Receipe(String name, int duration, String text) {
		this.name = name;
		this.duration = duration;
		this.text = text;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setText(String text) {
		this.text = text;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public String getText() {
		return text;
	}

	public String getName() {
		return name;
	}

	public int getDuration() {
		return duration;
	}

}
